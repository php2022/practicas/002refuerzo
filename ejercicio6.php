<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //opcion 1:
        //no es válido para el interior de las clases
        //define("NOMBRE","RAMON");
        
        //opcion 2
        //es válido siempre
        const NOMBRE="RAMON";
        
        echo NOMBRE;
        ?>
    </body>
</html>
