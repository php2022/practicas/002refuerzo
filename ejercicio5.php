<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $ancho=200;
        $alto=100;
        
        //opcion 1:
        echo "<div style=\"background-color:black;width:{$ancho}px;height:{$alto}px\></div>";
        //opcion 2:
        echo '<div style="background-color:black;width:' 
        . $ancho . 'px;height:' . $alto . 'px"></div>';
        ?>
    </body>
</html>
