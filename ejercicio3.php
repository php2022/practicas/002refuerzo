<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $radio=2.4;
        $perimetro=2*M_PI*$radio;
        $area=pi()*$radio**2;
        ?>
        <div>
            El radio del círculo es <?= $radio?>
        </div>
        <div>
            El área del círculo es <?= $area ?>
        </div>
        <div>
            El perímetro del círculo es <?= $perimetro ?>
        </div>
    </body>
</html>
